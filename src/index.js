import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './scss/style.scss';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
