import React, { useState, useEffect } from 'react'
import ProjectDetails from './components/ProjectDetails'
import SubscriptionBox from './components/SubscriptionBox'

function App() {
  const [data, setData] = useState([])
  const [visible, setVisible] = useState(false)
  const [project, setProject] = useState({})
  const getData = () => {
    fetch('data.json', {
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
      .then(function (response) {
        console.log(response)
        return response.json()
      })
      .then(function (myJson) {
        // console.log(myJson)
        setData(myJson)
      })
  }
  const showModal = s => {
    setVisible(true)
    console.log('called', true)
    setProject(s)
  }
  const hideModal = () => {
    setVisible(false)
    console.log('called', false)
    setProject({})
  }
  useEffect(() => {
    getData()
  }, [])

  return (
    <>
      <section className='mainContent'>
        <div style={{ textAlign: 'center' }}>
          <img
            alt='logo'
            src='./logo.jpg'
            style={{ width: '100px', height: '100px', marginBottom: '10px' }}
          />{' '}
          <h2>REUSABLE COMPONENTS</h2>
        </div>
        <div className='mainContent-in'>
          <div className='row'>
            <div className='col-12 col-md-12 '>
              {visible ? (
                <ProjectDetails hideModal={hideModal} data={project} />
              ) : (
                <div className='subscriptionBox-wrp'>
                  <div className='row'>
                    {data &&
                      data.length > 0 &&
                      data.map(s => (
                        <div className='col-12 col-md-6 col-lg-4' key={s.projectId}>
                          <SubscriptionBox
                            styleName='trialBox'
                            headtext={s.projectName}
                            Description={s.description}
                            showModal={e => showModal(s)}
                            id={s.projectId}
                            updateDate={s.updateDate}
                          />
                        </div>
                      ))}
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default App
