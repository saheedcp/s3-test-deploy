/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import { Button } from 'antd'
const SubscriptionBox = ({
  styleName,
  headtext,
  Description,
  showModal,
  updateDate,
  id,
}) => {
  return (
    <>
      <div className={`subscriptionBox ${styleName}`}>
        <div className='subscriptionBox-head'>
          <h3>{headtext}</h3>
        </div>
        <div id={id} className='subscriptionBox-body'>
          <h6>{Description}</h6>
          <h6>Last Updated date: {updateDate}</h6>
          <div className='subscriptionBoxbtn-wrp'>
            <Button className='cmnBtn blueBtn' onClick={showModal}>
              View
            </Button>
          </div>
        </div>
      </div>
     
    </>
  )
}

export default SubscriptionBox
