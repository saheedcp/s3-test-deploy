/* eslint-disable jsx-a11y/alt-text */
import React from 'react'
import { Button, Table, Badge } from 'antd'
const ProjectDetails = ({ hideModal, data }) => {
  const columns = [
    {
      title: 'URL',
      dataIndex: 'url',
      width: 300,
      render: (id, number) => (
        <span>
          {' '}
          <a
            href={number.url}
            style={{ color: ' #40a9ff' }}
            target='_blank'
            rel='noopener noreferrer'
          >
            {number.url}
          </a>
        </span>
      ),
    },
    {
      title: 'Name',
      dataIndex: 'name',
      width: 250,
    },
    {
      title: 'Branch',
      dataIndex: 'branch',
      width: 150,
    },
  ]

  let status = data.projectStatus
  const listStatus = status.map(s => (
    <>
      <Badge
        count={s}
        style={{
          backgroundColor:
            s.indexOf('In') > -1 ? 'green' : s.indexOf('Stopped') > -1 ? 'red' : '#ff8800',
          marginRight: '10px',
        }}
      />
    </>
  ))
  let names = data.pointOfContact

  const listNamess = [
    {
      title: 'Name',
      dataIndex: 'name',
      width: 300,
    },
    {
      title: 'Email',
      dataIndex: 'contactEmail',
      width: 250,
    },
  ]
  const getTech = module => {
    let technologies = module.technologies
    const listTechnologies = technologies.map(technology => (
      <span className='techElement'> {technology}</span>
    ))
    return listTechnologies
  }
  const getThirdParties = module => {
    let thirdParties = module.thirdParties
    const listThirdParties = thirdParties.map(t => <span className='element'> {t}</span>)
    return listThirdParties
  }
  let modulesArray = data.modules
  let moduleList = modulesArray.map(module => (
    <>
    <li>
              <h6>Module Name </h6>
              <span><h5 style={{color:'#40a9ff'}}> : {module.moduleName}</h5>

              
      <ul className='module'>
        {/* <h3 style={{textAlign:'center',color:'#40a9ff'}}>{module.moduleName}</h3> */}
        <li>
          <h6>Module Type</h6>
          <span>: {module.moduleType}</span>
        </li>
        <li>
          <h6>Technologies</h6>
          <span>: {getTech(module)}</span>
        </li>
        <li>
          <h6>Third Parties</h6>
          <span>: {getThirdParties(module)}</span>
        </li>
        <li>
          <h6>Functional Document Url</h6>
          <span>
            :{' '}
            <a
              href={module.functionalDocumentUrl}
              target='_blank'
              rel='noopener noreferrer'
              style={{ color: ' #40a9ff' }}
            >
              {module.functionalDocumentUrl}
            </a>
          </span>
        </li>
        <li>
          <h6>Technical Document Url</h6>
          <span>
            :{' '}
            <a
              href={module.technicalDocumentUrl}
              target='_blank'
              rel='noopener noreferrer'
              style={{ color: ' #40a9ff' }}
            >
              {module.technicalDocumentUrl}
            </a>
          </span>
        </li>
        <li>
          <h6>Sandbox Url</h6>
          <span>
            :{' '}
            <a
              href={module.sandboxUrl}
              target='_blank'
              rel='noopener noreferrer'
              style={{ color: ' #40a9ff' }}
            >
              {module.sandboxUrl}
            </a>
          </span>
        </li>
        <li>
          <h6>API Document Url</h6>
          <span>
            :{' '}
            <a
              href={module.apiDocumentUrl}
              target='_blank'
              style={{ color: ' #40a9ff' }}
              rel='noopener noreferrer'
            >
              {module.apiDocumentUrl}
            </a>
          </span>
        </li>
        <li>
          <h6>Repo Url</h6>
          <div className='table-wrp'>
            <Table
              columns={columns.sort()}
              dataSource={module.repoUrls}
              pagination={false}
              rowKey='id'
            ></Table>
          </div>
        </li>
      </ul>
      </span>
            </li>
    </>
  ))
  return (
    <>
      <div className='modalFooter'>
        <Button className='cmnBtn blueBtn' key='back' onClick={hideModal}>
          BACK
        </Button>
      </div>
      <div className='subscriptionBox'>
        <div className='SubDetails' style={{ textAlign: 'start' }}>
          <ul className='list-unstyled'>
            <li>
              <h6>Project Name</h6>
              <span><h5 style={{color:'#40a9ff'}}> : {data.projectName}</h5></span>
            </li>
            <li>
              <h6> Project Description</h6>
              <span>: {data.description}</span>
            </li>
            <li>
              <h6>Project Status</h6>
              <span>: {listStatus}</span>
            </li>
            <li>
              <h6>Service Type</h6>
              <span>: {data.serviceType}</span>
            </li>
            <li>
              <h6>Version</h6>
              <span>: {data.version}</span>
            </li>
            <li>
              <h6>Current Release Note</h6>
              <span>: {data.currentReleaseNote}</span>
            </li>
            <li>
              <h6>Updated By</h6>
              <span>: {data.updatedBy}</span>
            </li>
            <li>
              <h6>Update Date</h6>
              <span>: {data.updateDate}</span>
            </li>
            <li>
              <h6>Next Release Date</h6>
              <span>: {data.nextReleaseDate}</span>
            </li>
            <li>
              <h6>Point of Contact</h6>
              <div className='table-wrp'>
                <Table
                  columns={listNamess.sort()}
                  dataSource={names}
                  pagination={false}
                  rowKey='id'
                ></Table>
              </div>
            </li>
            <li>
              <div >{moduleList}</div>
            </li>
          </ul>

          {/* <div>{moduleList}</div> */}
        </div>
      </div>
    </>
  )
}

export default ProjectDetails
